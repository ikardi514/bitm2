<?php
namespace Mess\user;

class User{
    public $conn = '';

    public function __construct($db_con){
        $this->conn = $db_con;
    }

    public function getAllUsers(){
        global $DB_con;

        $query = 'SELECT * FROM users';
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $users=[];
        while($user = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $users[] = $user;
        }
        return $users;
    }

    public function store($data){
        if(!$this->isValidUser($data)){
            return false;
        }
        try{
            $stmt = $this->conn->prepare("INSERT INTO users(user_name,password,email) VALUES(:user_name, :password, :email)");
            $stmt->bindparam(":user_name",$data['user_name']);
            $stmt->bindparam(":password",md5($data['password']));
            $stmt->bindparam(":email",$data['email']);
            $stmt->execute();
            return true;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    public function show($data)
    {
        $id = $data['id'];
        try{
            $query = "SELECT * FROM users WHERE id =$id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount()> 0)
            {
                $row = $stmt->fetch(\PDO::FETCH_ASSOC);
                return $row;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    public function edit($data)
    {
        $id = $data['id'];
        try{
            $query = "SELECT * FROM users WHERE id =$id";
            $stmt = $this->conn->prepare($query);
            $stmt->bindparam(":id",$id);
            $stmt->execute();
            if ($stmt->rowCount()> 0)
            {
                $row = $stmt->fetch(\PDO::FETCH_ASSOC);
                return $row;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    public function update($data)
    {
        $userName = $data['username'];
        $password = md5($data['password']);
        $email = $data['email'];
        $id = $data['id'];
        try{
            $query = "UPDATE users SET user_name= :username, password=:password, email=:email WHERE id = $id";
            $stmt = $this->conn->prepare($query);
            $stmt->bindparam(':username',$userName);
            $stmt->bindparam(':password',$password);
            $stmt->bindparam(':email',$email);
            $stmt->execute();
            header('location:userView.php');

        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    public function deleteUser($id){
        try{
            $query = "DELETE FROM users WHERE id = $id";
            $stmt = $this->conn->prepare($query);
            $stmt->bindparam(":id",$id);
            $stmt->execute();
            return true;
        }catch(PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    private function isValidUser($data){

        $user_name= $data['user_name'];
        try{
            $query = "SELECT user_name FROM users WHERE user_name ='$user_name'";
           // echo $query; die();
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount()> 0)
            {$_SESSION['message'] = "User already exists.";
                header('location:userCreate.php');
            }

            else{
                $_SESSION['message'] = "User Added Successfully";
                header('location:userView.php');
                return true;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}