<?php
namespace Mess\bill;


class Bill{
    public $conn = '';

    public function __construct($db_con){
        $this->conn = $db_con;
    }

    public function getAllBills(){
        global $DB_con;
        $query = "SELECT b.id, b.balance_bf, b.maintenance, b.entertainment, b.mess_sports, b.garden, b.cos_bf, b.csf, b.crf, b.cwc, b.base_sports, b.bafwwa, b.barack_damage, b.wild_fund,b.fws, b.bhss, b.levey, b.cnf_loan, b.casual_meal, b.internet, b.others, b.total, m.bdno FROM bills b, mess_members m where b.member_id = m.id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $bills=[];
        while($bill = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $bills[] = $bill;
        }
        return $bills;
    }


    public function getAllBillsByDate($data){
        global $DB_con;

        $fromDate = $data['formDate'];
        $toDate = $data['toDate'];

//      var_dump($fromDate);
//      var_dump($toDate); die();

        $query = "SELECT b.id, b.balance_bf, b.maintenance, b.entertainment, b.mess_sports, b.garden, b.cos_bf, b.csf, b.crf, b.cwc, b.base_sports, b.bafwwa, b.barack_damage, b.wild_fund,b.fws, b.bhss, b.levey, b.cnf_loan, b.casual_meal, b.internet, b.others, b.total, m.bdno FROM bills b, mess_members m where b.member_id = m.id AND b.billing_date BETWEEN '$fromDate' AND '$toDate'";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $bills=[];
        while($bill = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $bills[] = $bill;
        }
        return $bills;
    }



    public function store($data){

        date_default_timezone_set('Asia/Dacca');

        $datetime = new \DateTime();
        $datetime = $datetime->format('Y-m-d');

        if(!$this->isValidBill($data)){
            return false;
        }

        $total = $this->getTotal($data);

        try{
            $stmt = $this->conn->prepare("INSERT INTO bills(balance_bf, maintenance, entertainment, mess_sports, garden, cos_bf, csf, crf, cwc, base_sports, bafwwa, 
barack_damage, wild_fund, fws, bhss, levey, cnf_loan, casual_meal, internet, others, billing_date, total, member_id) 
VALUES (:balance_bf, :maintenance, :entertainment, :mess_sports, :garden, :cos_bf, :csf, :crf, :cwc, :base_sports, :bafwwa, :barack_damage, :wild_fund, :fws, :bhss, :levey,:cnf_loan, :casual_meal, :internet, :others, :billing_date,:total, :member_id)");
            $stmt->bindparam(":balance_bf",$data['balance_bf']);
            $stmt->bindparam(":maintenance",$data['maintenance']);
            $stmt->bindparam(":entertainment",$data['entertainment']);
            $stmt->bindparam(":mess_sports",$data['mess_sports']);
            $stmt->bindparam(":garden",$data['garden']);
            $stmt->bindparam(":cos_bf",$data['cos_bf']);
            $stmt->bindparam(":csf",$data['csf']);
            $stmt->bindparam(":crf",$data['crf']);
            $stmt->bindparam(":cwc",$data['cwc']);
            $stmt->bindparam(":base_sports",$data['base_sports']);
            $stmt->bindparam(":bafwwa",$data['bafwwa']);
            $stmt->bindparam(":barack_damage",$data['barack_damage']);
            $stmt->bindparam(":wild_fund",$data['wild_fund']);
            $stmt->bindparam(":fws",$data['fws']);
            $stmt->bindparam(":bhss",$data['bhss']);
            $stmt->bindparam(":levey",$data['levey']);
            $stmt->bindparam(":cnf_loan",$data['cnf_loan']);
            $stmt->bindparam(":casual_meal",$data['casual_meal']);
            $stmt->bindparam(":internet",$data['internet']);
            $stmt->bindparam(":others",$data['others']);
            $stmt->bindparam(":billing_date",$datetime);
            $stmt->bindparam(":total",$total);
            $stmt->bindparam(":member_id",$data['member_id']);
            $stmt->execute();
            return true;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    protected function getTotal($data){
        return $data['balance_bf']+$data['maintenance']+$data['entertainment'] +$data['mess_sports'] +$data['garden'] +$data['cos_bf'] +$data['csf'] +$data['crf'] +$data['cwc'] +$data['base_sports'] +$data['bafwwa'] +$data['barack_damage'] +$data['wild_fund'] +$data['fws'] +$data['bhss'] +$data['levey'] +$data['cnf_loan'] +$data['casual_meal'] +$data['internet'] +$data['others'];
    }


    public function show($data)
    {
        if(!$this->isBillCreated($data)){
            return false;
        }
        $bdno = $data['bdno'];
        try{
            $query = "SELECT b.balance_bf, b.maintenance, b.entertainment, b.mess_sports, b.garden, b.cos_bf, b.csf, b.crf, b.cwc, b.base_sports, b.bafwwa, b.barack_damage, b.wild_fund,b.fws, b.bhss, b.levey, b.cnf_loan, b.casual_meal, b.internet, b.others, b.total, b.member_id, m.bdno , m.rank, m.name, m.trade FROM bills b, mess_members m where b.member_id = m.id AND m.bdno =$bdno";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount()> 0)
            {
                $row = $stmt->fetch(\PDO::FETCH_ASSOC);
                return $row;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }



    public function showPDF($data)
    {
        $bdno = $data['bdno'];
        try{
            $query = "SELECT b.balance_bf, b.maintenance, b.entertainment, b.mess_sports, b.garden, b.cos_bf, b.csf, b.crf, b.cwc, b.base_sports, b.bafwwa, b.barack_damage, b.wild_fund,b.fws, b.bhss, b.levey, b.cnf_loan, b.casual_meal, b.internet, b.others, m.bdno, b.total, m.rank, m.name, m.trade FROM bills b, mess_members m where b.member_id = m.id AND m.bdno =$bdno";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount()> 0)
            {
                $row = $stmt->fetch(\PDO::FETCH_ASSOC);
                return $row;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    public function edit($data)
    {

        $id = $data['id'];
        try {
            $query = "SELECT * FROM `bills` WHERE `id`=$id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                $row = $stmt->fetch(\PDO::FETCH_ASSOC);
                return $row;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    public function update($data)
    {
        echo $id = $data['id'];
        $total = $this->getTotal($data);
        try {
            $stmt = $this->conn->prepare("UPDATE `bills` SET
                                `balance_bf`=:balance_bf,
                                `maintenance`=:maintenance,
                                `entertainment`=:entertainment,
                                `mess_sports`=:mess_sports,
                                `garden`=:garden,
                                `cos_bf`=:cos_bf,
                                `csf`=:csf,
                                `crf`=:crf,
                                `cwc`=:cwc,
                                `base_sports`=:base_sports,
                                `bafwwa`=:bafwwa,
                                `barack_damage`=:barack_damage,
                                `wild_fund`=:wild_fund,
                                `fws`=:fws,
                                `bhss`=:bhss,
                                `levey`=:levey,
                                `cnf_loan`=:cnf_loan,
                                `casual_meal`=:casual_meal,
                                `internet`=:internet,
                                `others`=:others,
                                `total`=:total
                                 WHERE `id`=$id");
            $stmt->bindparam(":balance_bf", $data['balance_bf']);
            $stmt->bindparam(":maintenance", $data['maintenance']);
            $stmt->bindparam(":entertainment", $data['entertainment']);
            $stmt->bindparam(":mess_sports", $data['mess_sports']);
            $stmt->bindparam(":garden", $data['garden']);
            $stmt->bindparam(":cos_bf", $data['cos_bf']);
            $stmt->bindparam(":csf", $data['csf']);
            $stmt->bindparam(":crf", $data['crf']);
            $stmt->bindparam(":cwc", $data['cwc']);
            $stmt->bindparam(":base_sports", $data['base_sports']);
            $stmt->bindparam(":bafwwa", $data['bafwwa']);
            $stmt->bindparam(":barack_damage", $data['barack_damage']);
            $stmt->bindparam(":wild_fund", $data['wild_fund']);
            $stmt->bindparam(":fws", $data['fws']);
            $stmt->bindparam(":bhss", $data['bhss']);
            $stmt->bindparam(":levey", $data['levey']);
            $stmt->bindparam(":cnf_loan", $data['cnf_loan']);
            $stmt->bindparam(":casual_meal", $data['casual_meal']);
            $stmt->bindparam(":internet", $data['internet']);
            $stmt->bindparam(":others", $data['others']);
            $stmt->bindparam(":total",$total);

            $stmt->execute();
            if ($stmt) {
                $_SESSION['message'] = "Bill Updated Successfully !!";
                header('location:billView.php');
            } else {
                return true;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }



    private function isValidBill($data){

        //unique for a bdnumber in a specific month of a year
        date_default_timezone_set('Asia/Dacca');

        $datetime = new \DateTime();
        $year = $datetime->format('Y');
        $month = $datetime->format('m');
//
//
//        //echo $date;
//        //list($year, $month, $day) = explode('-',$date);
//        var_dump($year);
//        var_dump($month);


        $member_id = $data['member_id'];
        try{
            $query = "SELECT * FROM bills WHERE member_id =$member_id AND YEAR(billing_date) = '$year' AND MONTH(billing_date) = '$month'";
           //echo $query;
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount()> 0)
            {
                $_SESSION['message'] = "Bill already created for this month.";
                header('location:billCreateHead.php');
            }

            else{
                $_SESSION['message'] = "Bill created Successfully";
                header('location:billView.php');
                return true;
            }

        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    private function isBillCreated($data){

        $bdno = $data['bdno'];
        try{
            $query = "SELECT m.id, m.bdno, b.member_id FROM mess_members m, bills b WHERE m.id = b.member_id AND bdno =$bdno";

           // echo $query; die();

            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount()== 0)
            {
                $_SESSION['message'] = "Bill does not created yet !!";
                header('location:billCreateHead.php');
            }
            else{
                return true;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}