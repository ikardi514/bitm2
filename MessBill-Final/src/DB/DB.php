<?php
namespace Mess\DB;

class DB {
    const DB_HOST = 'localhost';
    const DB_NAME = 'mess_bill';
    const DB_USER = 'root';
    const DB_PASSWORD = '';

    public static $conn = '';


    public static function connect(){
        try
        {
            self::$conn = new \PDO("mysql:host=".self::DB_HOST.";dbname=".self::DB_NAME,self::DB_USER,self::DB_PASSWORD);
            self::$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
} 