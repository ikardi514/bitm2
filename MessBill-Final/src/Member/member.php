<?php
namespace Mess\member;


class Member{
    public $conn = '';

    public function __construct($db_con){
        $this->conn = $db_con;

    }

    public function getAllMembers(){
        global $DB_con;

        $query = 'SELECT * FROM mess_members ORDER BY LENGTH(entry_no), entry_no ASC, bdno ASC';
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $members=[];
        while($member = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $members[] = $member;
        }
        return $members;
    }

    public function store($data){
        if(!$this->isValidBdno($data)){
            return false;
        }

        try{
            $stmt = $this->conn->prepare("INSERT INTO mess_members(bdno,rank,name,trade,entry_no) VALUES(:bdno, :rank, :name, :trade, :entryno)");
            $stmt->bindparam(":bdno",$data['bdno']);
            $stmt->bindparam(":rank",$data['rank']);
            $stmt->bindparam(":name",$data['name']);
            $stmt->bindparam(":trade",$data['trade']);
            $stmt->bindparam(":entryno",$data['entryno']);
            $stmt->execute();
            return true;
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

    public function show($data)
    {
        $id = $data['id'];
        try{
            $query = "SELECT * FROM mess_members WHERE id =$id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount()> 0)
            {
                $row = $stmt->fetch(\PDO::FETCH_ASSOC);
                return $row;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    public function searchMember($data)
    {
        if(!$this->isMessMember($data)){
            return true;
        }

        $bdno = $data['bdno'];
        try{
            $query = "SELECT id, bdno FROM mess_members WHERE bdno =$bdno";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount()> 0)
            {
                $row = $stmt->fetch(\PDO::FETCH_ASSOC);
                return $row;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    public function edit($data)
    {
        $id = $data['id'];
        try{
            $query = "SELECT * FROM mess_members WHERE id =$id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount()> 0)
            {
                $row = $stmt->fetch(\PDO::FETCH_ASSOC);
                return $row;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    public function update($data)
    {
        $id = $data['id'];
        $bdno = $data['bdno'];
        $rank = $data['rank'];
        $name = $data['name'];
        $trade = $data['trade'];
        $entryno = $data['entryno'];

        try{
            $query = "UPDATE mess_members SET id= :id, bdno= :bdno, rank=:rank, name=:name, trade=:trade, entry_no=:entryno WHERE id = $id";
            $stmt = $this->conn->prepare($query);
            $stmt->bindparam(':id',$id);
            $stmt->bindparam(':bdno',$bdno);
            $stmt->bindparam(':rank',$rank);
            $stmt->bindparam(':name',$name);
            $stmt->bindparam(':trade',$trade);
            $stmt->bindparam(':entryno',$entryno);
            $stmt->execute();
            header('location:memberView.php');

        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    public function deleteMember($id){
        try{
            $query = "DELETE FROM mess_members WHERE id = $id";
            $stmt = $this->conn->prepare($query);
            $stmt->bindparam(":id",$id);
            $stmt->execute();
            return true;
        }catch(PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }


    private function isValidBdno($data){

        $bdno = $data['bdno'];
        try{
            $query = "SELECT bdno FROM mess_members WHERE bdno =$bdno";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount()> 0)
            {
                $_SESSION['message'] = "BD NO already exists.";
                header('location:memberCreate.php');
            }
            else{
                $_SESSION['message'] = "Member Added Successfully";
                header('location:memberView.php');
                return true;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    private function isMessMember($data){

        $bdno = $data['bdno'];
        try{
            $query = "SELECT bdno FROM mess_members WHERE bdno =$bdno";

            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            if ($stmt->rowCount()< 1)
            {
                $_SESSION['message'] = "You are not Member Yet !!.";
                header('location:billCreateHead.php');
            }

            else{
                return true;
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}