<?php
namespace Mess\Utility;

class Setting {

    public static $app_mode = 'prod';

    public static function init(){

        if(self::$app_mode == 'dev'){
            ini_set('display_errors','On');
        }else{
            ini_set('display_errors','Off');
        }
    }
}