<?php
include ('session.php');
require_once('Header.php');
?>

<form action="userStore.php" method="post">
    <div class="row">
       <div class="mainpage">
           <?php
           if(isset($_SESSION['message'])){
               echo "<div class='alert alert-danger col-md-6 col-md-offset-4 text-center'> ".$_SESSION['message']."</div>";
               unset($_SESSION['message']);
           }
           ?>
        <section>
            <h2 style="text-align: center">ADD CLERK</h2><br />
            <div class="form-horizontal">
               <div class="form-group">
                  <label for="userName" class="col-md-2 col-md-offset-2 control-label">User Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="userName" placeholder="User Name" name="user_name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-md-2 col-md-offset-2 control-label">Password</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-2 col-md-offset-2 control-label">Email</label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" id="email" placeholder="Email" name="email">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button class="btn btn-primary" type="submit" name="sub">ADD CLERK</button>
                            <button class="btn btn-danger" type="reset" name="sub">CLEAR</button>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</form>


<?php
    require_once('Footer.php');
?>