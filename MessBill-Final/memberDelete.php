<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\Utility\Setting;
use Mess\DB\DB;
use Mess\Member\member;

Setting::init();
DB::connect();

$member = new Member(DB::$conn);


if (isset($_POST['delete'])  == TRUE) {
    $id = (int)$_POST['delete'];
    if ($member->deleteMember($id)) {
        header("location: memberView.php");
    }

}