<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\Utility\Setting;
use Mess\DB\DB;
use Mess\User\user;

Setting::init();
DB::connect();

$user = new User(DB::$conn);
$user = $user->edit($_GET);

require_once('Header.php');

?>

<form action="userUpdate.php" method="post">
    <div class="row">
        <section>
            <h2 style="text-align: center">EDIT : USER</h2>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="id" class="col-md-2 col-md-offset-2 control-label">ID</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="id" value="<?php echo $user['id'];?>" name="id"  readonly="readonly">
                    </div>
                </div>

                <div class="form-group">
                    <label for="username" class="col-md-2 col-md-offset-2 control-label">User Name</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="username" value="<?php echo $user['user_name'];?>" name="username">
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="col-md-2 col-md-offset-2 control-label">Password</label>
                    <div class="col-md-6">
                        <input type="password" class="form-control" id="password" value="<?php echo $user['password'];?>" name="password">
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-md-2 col-md-offset-2 control-label">Email</label>
                    <div class="col-md-6">
                        <input type="email" class="form-control" id="email" value="<?php echo $user['email'];?>" name="email">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button class="btn btn-primary" type="submit" name="sub">Update</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
</form>
<?php
require_once('Footer.php');
?>