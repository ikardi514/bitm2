<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\DB\DB;
use Mess\User\user;

DB::connect();

$user= new User(DB::$conn);
$user = $user->show($_GET);

require_once('Header.php');
?>


<div class="row">
    <div class="mainpage">
        <section class="col-md-8 col-md-offset-2">
            <table class="table table-striped table-bordered">
                <tr>
                    <th>User Name</th>
                    <th>Password</th>
                    <th>Email</th>
                </tr>

                <tr>
                    <td> <?php echo $user['user_name'] ?> </td>
                    <td> <?php echo $user['password'] ?> </td>
                    <td> <?php echo $user['email'] ?> </td>
                </tr>
            </table>
        </section>
    </div>
</div>

<?php
require_once('Footer.php');
?>
