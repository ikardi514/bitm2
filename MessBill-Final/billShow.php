<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\DB\DB;
use Mess\Bill\bill;

DB::connect();

$bill= new Bill(DB::$conn);
$bill = $bill->show($_POST);

require_once('Header.php');
?>


<div class="row">
    <div class="mainpage">
        <section class="col-md-8 col-md-offset-2">

            <div class="tablepage">
                <table class="table table-bordered ">
                    <tr><td colspan="2"><h2 style="text-align: center">INDEVISUAL BILL</h2></td></tr>
                    <tr>
                        <td>BD Number : <?php echo $bill['bdno'] ?></td>
                        <td>Name : <?php echo $bill['name'] ?></td>
                    </tr>
                    <tr>
                        <td>Rank : <?php echo $bill['rank'] ?></td>
                        <td>Trade : <?php echo $bill['trade'] ?></td>
                    </tr>
                </table>
            </div>

            <table class="table table-bordered table-striped ">
                <tr>
                    <td>Banalce BF</td>
                    <td><?php echo $bill['balance_bf'] ?></td>

                    <td>Maintenance</td>
                    <td><?php echo $bill['maintenance'] ?></td>
                </tr>
                <tr>
                    <td>Entertainment</td>
                    <td><?php echo $bill['entertainment'] ?></td>

                    <td>Mess Sports</td>
                    <td><?php echo $bill['mess_sports'] ?></td>
                </tr>
                <tr>
                    <td>Garden</td>
                    <td><?php echo $bill['garden'] ?></td>

                    <td>CO'S BF </td>
                    <td><?php echo $bill['cos_bf'] ?></td>
                </tr>
                <tr>
                    <td>CSF</td>
                    <td><?php echo $bill['csf'] ?></td>

                    <td>CRF </td>
                    <td><?php echo $bill['crf'] ?></td>
                </tr>
                <tr>
                    <td>CWC</td>
                    <td><?php echo $bill['cwc'] ?></td>

                    <td>Base Sports </td>
                    <td><?php echo $bill['base_sports'] ?></td>
                </tr>
                <tr>
                    <td>BAFWWA</td>
                    <td><?php echo $bill['bafwwa'] ?></td>

                    <td>Barrack Damage</td>
                    <td><?php echo $bill['barack_damage'] ?></td>
                </tr>
                <tr>
                    <td>Wild Converstion Fund</td>
                    <td><?php echo $bill['wild_fund'] ?></td>

                    <td>FWS</td>
                    <td><?php echo $bill['fws'] ?></td>
                </tr>
                <tr>
                    <td>BHSS</td>
                    <td><?php echo $bill['bhss'] ?></td>

                    <td>Levey / Party</td>
                    <td><?php echo $bill['levey'] ?></td>
                </tr>
                <tr>
                    <td>CNF Loan</td>
                    <td><?php echo $bill['cnf_loan'] ?></td>

                    <td>Casual Meal</td>
                    <td><?php echo $bill['casual_meal'] ?></td>
                </tr>
                <tr>
                    <td>Internet Bill</td>
                    <td><?php echo $bill['internet'] ?></td>

                    <td>Others</td>
                    <td><?php echo $bill['others'] ?></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right">Total Bill :</td>
                    <td colspan="3"><?php echo $bill['total']?>
                        &nbsp; &nbsp;<form action="billShowPDF.php" method="post" style="display: inline-block">
                                <input type="hidden" value="<?php echo $bill['bdno'] ?>" name="bdno">
                            <button type="submit"><img src="images/pdf.png" title="Download as PDF"></button>
                        </form>

                    </td>
                </tr>

            </table>
        </section>
    </div>
</div>

<?php
require_once('Footer.php');
?>
