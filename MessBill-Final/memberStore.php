<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\Utility\Setting;
use Mess\Utility\Sanitize;
use Mess\DB\DB;
use Mess\Member\member;

Setting::init();
DB::connect();

?>

<?php
$data = Sanitize::sanitize($_POST);
$member = new Member(DB::$conn);
$member->store($data);

?>

