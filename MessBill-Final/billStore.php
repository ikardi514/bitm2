<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\Utility\Setting;
use Mess\Utility\Sanitize;
use Mess\DB\DB;
use Mess\Bill\bill;

Setting::init();
DB::connect();

?>

<?php
$data = Sanitize::sanitize($_POST);
//var_dump($data); die();

$bill = new Bill(DB::$conn);
$bill->store($data);

?>

