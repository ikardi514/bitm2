<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\DB\DB;
use Mess\Member\member;

DB::connect();

$member= new Member(DB::$conn);
$member = $member->show($_GET);

require_once('Header.php');
?>


<div class="row">
    <div class="mainpage">
        <section class="col-md-8 col-md-offset-2">
            <table class="table table-striped table-bordered">

                <tr>
                    <th>BD Number</th>
                    <th>Rank</th>
                    <th>Name</th>
                    <th>Trade</th>
                    <th>Entry Number</th>
                </tr>

                <tr>
                    <td> <?php echo $member['bdno'] ?> </td>
                    <td> <?php echo $member['rank'] ?> </td>
                    <td> <?php echo $member['name'] ?> </td>
                    <td> <?php echo $member['trade'] ?> </td>
                    <td> <?php echo $member['entry_no'] ?> </td>
                </tr>
            </table>
        </section>
    </div>
</div>

<?php
require_once('Footer.php');
?>
