<?php
require_once('Login_header.php');
?>



<form action="" method="post">
    <div class="row">
        <div class="mainpage">
            <section>

                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="username" class="col-md-2 col-md-offset-2 control-label">User Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="username" placeholder="User Name" name="username">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-md-2 col-md-offset-2 control-label">Password</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button class="btn btn-lg btn-primary" type="submit" name="login">Login</button>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</form>


<?php
require_once('Footer.php');
?>