<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\Utility\Setting;
use Mess\DB\DB;
use Mess\Member\member;

Setting::init();
DB::connect();

$member = new Member(DB::$conn);
$member = $member->edit($_GET);

require_once('Header.php');

?>

    <form action="memberUpdate.php" method="post">
        <div class="row">
            <div class="mainpage">
                <section>
                    <h2 style="text-align: center">EDIT : MESS MEMBER</h2><br />
                    <div class="form-horizontal">

               <input type="hidden"  name="id"  value="<?php echo $member['id'];?>">


                        <div class="form-group">
                            <label for="bdno" class="col-md-2 col-md-offset-2 control-label">BD Number</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="bdno" readonly="readonly" name="bdno" value="<?php echo $member['bdno'];?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 col-md-offset-2 control-label" for="rank">Rank</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="rank" name="rank" required value="<?php echo $member['rank'];?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-2 col-md-offset-2 control-label">Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $member['name'];?>" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-2 col-md-offset-2 control-label" for="trade">Trade</label>
                            <div class="col-md-6"><input type="text" class="form-control" id="trade" name="trade" value="<?php echo $member['trade'];?>" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="entryno" class="col-md-2 col-md-offset-2 control-label">Entry No</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="entryno" value="<?php echo $member['entry_no'];?>" name="entryno">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button class="btn btn-lg btn-primary" type="submit" name="sub">UPDATE</button>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </form>
<?php
require_once('Footer.php');
?>