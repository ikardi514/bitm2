<?php
include ('session.php');
require_once('Header.php');
?>

<?php
if(isset($_SESSION['message'])){
    echo "<div class='alert alert-danger col-md-8 col-md-offset-2 text-center'> ".$_SESSION['message']."</div>";
    unset($_SESSION['message']);
}
?>

    <form action="billShow.php" method="post">
        <div class="row">
            <div class="mainpage">
                <section>
                    <h2 style="text-align: center">INDEVISUAL BILL</h2><br />
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="bdno" class="col-md-2 col-md-offset-2 control-label">BD Number</label>
                            <div class="col-md-4">
                                <input type="number" class="form-control" id="bdno" placeholder="BD Number" name="bdno" required>
                            </div>
                            <div class="col-md-2">
                                <button  type="submit" name="sub" class="btn btn-primary ">Search</button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>


<?php
require_once('Footer.php');
?>