<?php
include ('session.php');
require_once('Header.php');
?>

    <form action="billViewDate.php" method="post">
        <div class="row">
            <div class="mainpage">
                <section>
                    <h2 style="text-align: center">SEARCH : BILL</h2><br />
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 col-md-offset-2 control-label">From Date</label>
                        <div class="col-md-4">
                            <input type="date" class="form-control" name="formDate">
                        </div>
                     </div>
                    </div>

                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 col-md-offset-2 control-label">To Date</label>
                            <div class="col-md-4">
                                <input type="date" class="form-control" name="toDate">
                            </div>
                        </div>
                    </div>

                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                 <button  type="submit" name="sub" class="btn btn-primary ">Search</button>
                            </div>
                        </div>
                    </div>



                </section>
            </div>
        </div>
    </form>


<?php
require_once('Footer.php');
?>