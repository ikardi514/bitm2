<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\Utility\Setting;
use Mess\DB\DB;
use Mess\Member\member;

Setting::init();
DB::connect();

$member= new Member(DB::$conn);
$members = $member->getAllmembers();

require_once('Header.php');
?>

<div class="row">
    <div class="mainpage">
        <?php
        if(isset($_SESSION['message'])){
            echo "<div class='alert alert-success col-md-8 col-md-offset-2 text-center'> ".$_SESSION['message']."</div>";
            unset($_SESSION['message']);
        }
        ?>

    <section class="col-md-8 col-md-offset-2">
        <table  id="omar" class="table table-bordered table-hover table-striped" >
            <tr><td colspan="6"><h2 style="text-align: center">ALL MESS MEMBER</h2></td></tr>
            <tr>
                <th>BD Number</th>
                <th>Rank</th>
                <th>Name</th>
                <th>Trade</th>
                <th>Entry No</th>
                <th>Action &nbsp; &nbsp;<a href="memberViewPDF.php" target="_blank"><img src="images/pdf.png" title="Download as PDF"></a></th>
            </tr>

           
			<?php
				foreach($members as $member){
            ?>
            
                <tr>
                    <td> <?php echo $member['bdno'] ?> </td>
                    <td> <?php echo $member['rank'] ?> </td>
                    <td> <?php echo $member['name'] ?> </td>
                    <td> <?php echo $member['trade'] ?> </td>
                    <td> <?php echo $member['entry_no'] ?> </td>
                    <td>

                        <div class="iconInline">

                            <a href="memberEdit.php?id=<?php echo $member['id'];?>"><button  type="submit"><img src="images/edit.png" title="Edit"/></button></a>

                        </div>

                        <div class="iconInline">
                            <form action="memberDelete.php" method="post">
                                <input  type="hidden" name="delete"  value="<?php echo $member['id'];?>"/>
                                <button  type="submit"><img src="images/delete.png" title="Delete"/></button>
                            </form>
                        </div>
                        <div class="iconInline">
                            <a href="memberShow.php?id=<?php echo $member['id'];?>"><button  type="submit"><img src="images/show.png" title="Show" /></button></a>
                        </div>
                        </td>

                </tr>

                <?php
            }
            ?>
        </table>
    </section>
</div>
</div>
<?php
require_once('Footer.php');
?>
