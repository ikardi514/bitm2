<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Mess Bill Information System</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/style.css">

</head>
<body>

<div class="container">
    <div class="row">
        <div>
            <header class="header">
                <div class="headerImage"><a href="Main.php"><img src="images/logo.png" title="BAF Mess"></a></div>
                <div class="headerText">  <h1 class="text-center">MESS BILL INFORMATION SYSTEM</h1>
                    <h3 class="text-center">BANGLADESH AIRFORCE BASE BASHAR</h3></div>

            </header>
        </div>
    </div>

    <div class="row">

            <nav class="navbar  navbar-default">
                <ul class="nav navbar-nav">
                    <li><a href="Main.php">Home</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">Setup <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="memberCreate.php">Add Mess Member</a></li>
                            <li class="divider"></li>
                            <li><a href="userCreate.php">Add Cleark</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">Form <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="billCreateHead.php">Create Bill</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">Report <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="memberView.php">Mess Members</a></li>
                            <li class="divider"></li>
                            <li><a href="billShowHead.php">Indevidual Bill</a></li>
                            <li class="divider"></li>
                            <li><a href="billViewHead.php">Search Bill</a></li>
                            <li class="divider"></li>
                            <li><a href="billView.php">All Bill</a></li>
							<li class="divider"></li>
                            <li><a href="userView.php">Users</a></li>
                        </ul>
                    </li>

                    <li><a href="Logout.php">Logout</a></li>
                </ul> 
            </nav>

    </div>
