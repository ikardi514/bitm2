-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2017 at 11:26 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mess_bill`
--

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE IF NOT EXISTS `bills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `balance_bf` float DEFAULT NULL,
  `maintenance` float DEFAULT NULL,
  `entertainment` float DEFAULT NULL,
  `mess_sports` float DEFAULT NULL,
  `garden` float DEFAULT NULL,
  `cos_bf` float DEFAULT NULL,
  `csf` float DEFAULT NULL,
  `crf` float DEFAULT NULL,
  `cwc` float DEFAULT NULL,
  `base_sports` float DEFAULT NULL,
  `bafwwa` float DEFAULT NULL,
  `barack_damage` float DEFAULT NULL,
  `wild_fund` float DEFAULT NULL,
  `fws` float DEFAULT NULL,
  `bhss` float DEFAULT NULL,
  `levey` float DEFAULT NULL,
  `cnf_loan` float DEFAULT NULL,
  `casual_meal` float DEFAULT NULL,
  `internet` float DEFAULT NULL,
  `others` float NOT NULL,
  `billing_date` date DEFAULT NULL,
  `total` float NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `bills`
--

INSERT INTO `bills` (`id`, `balance_bf`, `maintenance`, `entertainment`, `mess_sports`, `garden`, `cos_bf`, `csf`, `crf`, `cwc`, `base_sports`, `bafwwa`, `barack_damage`, `wild_fund`, `fws`, `bhss`, `levey`, `cnf_loan`, `casual_meal`, `internet`, `others`, `billing_date`, `total`, `member_id`) VALUES
(33, 10, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 0, 0, 0, 0, 10, 0, 10, '2017-02-02', 94, 71),
(34, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 20, '2017-03-01', 210, 74),
(35, 10, 20, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 30, 10, 10, 10, 10, 400, 200, '2017-02-03', 764, 75),
(36, 0, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 0, 0, 0, 0, 0, 0, 30, '2017-03-01', 94, 76),
(37, 0, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 5, 0, 5, 0, 0, 0, 10, '2017-03-03', 84, 77),
(38, 10, 10, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 0, 0, 0, 0, 0, 0, 0, '2017-02-07', 84, 62),
(39, 11, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 0, 0, 0, 0, 0, 0, 0, '2017-03-05', 75, 63),
(40, 0, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 0, 0, 0, 100, 0, 0, 0, '2017-03-07', 164, 64),
(41, 0, 10, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 10, 0, 0, 0, 0, 0, 0, '2017-03-09', 84, 65),
(42, 0, 10, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 10, 0, 0, 0, 0, 0, 0, '2017-02-03', 84, 66),
(43, 0, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 0, 0, 0, 0, 0, 0, 0, '2017-03-18', 64, 67),
(44, 0, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 200, 0, 100, 10, 0, 0, 0, '2017-03-03', 374, 68),
(45, 0, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 0, 0, 0, 0, 10, 0, 0, '2017-03-03', 74, 59),
(46, 0, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 0, 0, 0, 0, 0, 0, 0, '2017-03-25', 64, 56),
(47, 0, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 10, 0, 0, 0, 0, 0, 0, '2017-03-03', 74, 69),
(48, 0, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 150, 0, 0, 0, 0, 0, 0, '2017-03-08', 214, 70),
(49, 0, 100, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 0, 0, 0, 0, 0, 0, 0, '2017-03-03', 164, 55),
(50, 0, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 0, 0, 300, 0, 0, 0, 0, '2017-03-10', 364, 57),
(51, 0, 10, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 0, 0, 0, 0, 10, 0, 0, '2017-03-12', 84, 78),
(52, 0, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 0, 0, 0, 0, 0, 0, 300, '2017-02-03', 364, 79),
(53, 0, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 10, 0, 0, 0, 10, 0, 0, '2017-03-18', 84, 80),
(54, 0, 0, 2, 3, 1, 12, 16, 10, 7, 1, 1, 1, 10, 101, 0, 10, 0, 11, 0, 100, '2017-03-15', 286, 81);

-- --------------------------------------------------------

--
-- Table structure for table `mess_members`
--

CREATE TABLE IF NOT EXISTS `mess_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bdno` varchar(255) NOT NULL,
  `rank` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `trade` varchar(255) NOT NULL,
  `entry_no` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `modified_at` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `mess_members`
--

INSERT INTO `mess_members` (`id`, `bdno`, `rank`, `name`, `trade`, `entry_no`, `created_at`, `modified_at`) VALUES
(55, '466280', 'Sgt', 'Omar', 'GS', '32', '', ''),
(56, '466281', 'Sgt', 'omar Jahan', 'GS', '33', '', ''),
(57, '465904', 'Sgt', 'Helal', 'Radio', '31', '', ''),
(58, '462719', 'WO', 'Hasem', 'Radio', '21', '', ''),
(59, '465188', 'Sgt', 'Sharif', 'Radio', '29', '', ''),
(60, '464056', 'Sgt', 'Noor', 'GS', '24', '', ''),
(62, '466201', 'SWO', 'Al Faruk', 'GS', '16', '', ''),
(63, '466202', 'WO', 'Amran', 'Clerk', '17', '', ''),
(64, '466203', 'WO', 'Amran', 'Radio', '18', '', ''),
(65, '466204', 'WO', 'Rafique', 'Clerk', '19', '', ''),
(66, '466206', 'SWO', 'Rahim Md Jahangir', 'Engine', '20', '', ''),
(67, '466209', 'Sgt', 'Karim', 'AC Fitt', '35', '', ''),
(68, '466210', 'Sgt', 'Imtiaz', 'AC Fitt', '21', '', ''),
(69, '466211', 'MWO', 'Omar Jahan', 'AC Fitt', '36', '', ''),
(70, '455656', 'WO', 'Maruf', 'AC Fitt', '39', '', ''),
(71, '471496', 'MWO', 'Rashed', 'Radio', '42', '', ''),
(74, '450000', 'MWO', 'Omar Faruq', 'GS', '15', '', ''),
(75, '465918', 'Sgt', 'Bahar', 'Radio', '31', '', ''),
(76, '466299', 'MWO', 'Nurul Islam', 'Engine', '20', '', ''),
(77, '450050', 'MWO', 'Delwar Hosain', 'Radar', '32', '', ''),
(78, '467100', 'WO', 'Sayeed', 'GS', '35', '', ''),
(79, '467101', 'MWO', 'Solaiman', 'Radio', '100', '', ''),
(80, '467102', 'MWO', 'Hassan', 'AC Fitt', '99', '', ''),
(81, '466279', 'SWO', 'Ramjan', 'GS', '32', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `modified_at` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `password`, `email`, `created_at`, `modified_at`) VALUES
(24, 'orange', '98f13708210194c475687be6106a3b84', 'mango@gmail.com', '', ''),
(25, 'omar', '92c8c96e4c37100777c7190b76d28233', 'omar_baf@yahoo.com', '', ''),
(26, 'helal', 'f47d0ad31c4c49061b9e505593e3db98', 'helal@gmail.com', '', ''),
(27, 'sohel', '202cb962ac59075b964b07152d234b70', 'sohel@gmail.com', '', ''),
(28, 'delowar', '202cb962ac59075b964b07152d234b70', 'delowar@gamil.com', '', ''),
(29, 'nazmul', '202cb962ac59075b964b07152d234b70', 'nazmul@gmail.com', '', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bills`
--
ALTER TABLE `bills`
  ADD CONSTRAINT `bills_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `mess_members` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
