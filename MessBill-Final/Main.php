<?php
include ('session.php');
require_once('Header.php');
?>


<div class="row">
    <section  class="col-md-8 col-md-offset-2">
        <div class="mainpage first-page">
         <h1 class="text-center">MESS BILL INFORMATION SYSTEM</h1>
            <?php
            if(isset($_SESSION['message'])){
                echo "<h2 class='text-center'>Login as : ".$_SESSION['message']."</h2>" ;
                unset ($_SESSION['message']);
            }
            ?>
        </div>
    </section>
</div>

<?php
require_once('Footer.php');
?>