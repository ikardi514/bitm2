<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\Utility\Setting;
use Mess\DB\DB;
use Mess\Bill\bill;

use Kilte\Pagination\Pagination;

Setting::init();
DB::connect();

$bill= new Bill(DB::$conn);
$bills = $bill->getAllBills();

$totalItems = count($bills);
$currentPage = 1;
if(array_key_exists('page',$_GET) && is_numeric($_GET['page'])){
    $currentPage = $_GET['page'];
}
$itemsPerPage = 8;

$pagination = new Pagination($totalItems,$currentPage, $itemsPerPage);

$offset = $pagination->offset();
$limit = $pagination->limit();
$pages = $pagination->build();

require_once('Header.php');
?>

<?php
if(isset($_SESSION['message'])){
    echo "<div class='alert alert-success col-md-8 col-md-offset-2 text-center'> ".$_SESSION['message']."</div>";
    unset($_SESSION['message']);
}
?>

<div class="row">
    <div class="mainpage ">
    <section class="col-md-12 allDataTable">
        <table class="table table-striped table-bordered">
            <tr><td colspan="23"><h2 style="text-align: center">ALL MEMBERS BILL</h2></td></tr>
            <tr>
                <th>BD No</th>
                <th>BL</th>
                <th>Maint</th>
                <th>Ent</th>
                <th>Mess-Sports</th>
                <th>Garden</th>
                <th>Co's BF</th>
                <th>CSF</th>
                <th>CRF</th>
                <th>CWC</th>
                <th>Base-Sports</th>
                <th>BAFWWA</th>
                <th>Brk-dmg</th>
                <th>Wild</th>
                <th>FWS</th>
                <th>BHSS</th>
                <th>Party</th>
                <th>LOAN</th>
                <th>Meal</th>
                <th>Internet</th>
                <th>Others</th>
                <th>Total</th>
                <th>Action</th>
            </tr>
		<?php
			for($counter=$offset; $counter < ($offset+$limit); $counter++){
            if(!array_key_exists($counter,$bills)){
                break;
            }
            ?>
            <tr>
               <td> <?php echo $bills[$counter]['bdno'];?></td>
                    <td> <?php echo $bills[$counter]['balance_bf'];?></td>
                    <td> <?php echo $bills[$counter]['maintenance'];?></td>
                    <td> <?php echo $bills[$counter]['entertainment'];?></td>
                    <td> <?php echo $bills[$counter]['mess_sports'];?></td>
                    <td> <?php echo $bills[$counter]['garden'];?></td>
                    <td> <?php echo $bills[$counter]['cos_bf'];?></td>
                    <td> <?php echo $bills[$counter]['csf'];?></td>
                    <td> <?php echo $bills[$counter]['crf'];?></td>
                    <td> <?php echo $bills[$counter]['cwc'];?></td>
                    <td> <?php echo $bills[$counter]['base_sports'] ?> </td>
                    <td> <?php echo $bills[$counter]['bafwwa'] ?> </td>
                    <td> <?php echo $bills[$counter]['barack_damage'] ?> </td>
                    <td> <?php echo $bills[$counter]['wild_fund'] ?> </td>
                    <td> <?php echo $bills[$counter]['fws'] ?> </td>
                    <td> <?php echo $bills[$counter]['bhss'] ?> </td>
                    <td> <?php echo $bills[$counter]['levey'] ?> </td>
                    <td> <?php echo $bills[$counter]['cnf_loan'] ?> </td>
                    <td> <?php echo $bills[$counter]['casual_meal'] ?> </td>
                    <td> <?php echo $bills[$counter]['internet'] ?> </td>
                    <td> <?php echo $bills[$counter]['others'] ?> </td> 
                    <td> <?php echo $bills[$counter]['total'] ?> </td>
                    <td>
                        <form action="billEdit.php" method="post">
                            <input type="hidden" name="id"
                                   value="<?php echo $bills[$counter]['id']?>">
                            <button type="submit"><img src="images/edit.png" title="Edit"/></button>
                        </form>

                        <form action="billShowPDF.php" method="post">
                            <input type="hidden" name="bdno"
                                   value="<?php echo $bills[$counter]['bdno']?>">
                            <button type="submit"><img src="images/pdf.png" title="Print"/></button>
                        </form>

                    </td>
            </tr>
        <?php
        }
        ?>
        </table>
		<ul>
		<?php 
			foreach($pages as $p=>$t){
				
				if($p == $currentPage){
                       echo "<li class='page active'>&nbsp;".$p."&nbsp;</li>";
                   }else{
				echo "<li class='page'>&nbsp;<a href='billView.php?page=".$p."'>".$p."</a>&nbsp;</li>";
			}}
		?>
		</ul>
		
        <h2 style="text-align: right">
        <form action="billViewPDF.php" method="post" >
            <button type="submit"><img src="images/pdf.png" title="Download as PDF"></button>
        </form></h2>
    </section>
</div>
</div>
<?php
require_once('Footer.php');
?>
