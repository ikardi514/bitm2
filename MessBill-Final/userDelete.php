<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\Utility\Setting;
use Mess\DB\DB;
use Mess\User\user;

Setting::init();
DB::connect();

$user = new User(DB::$conn);


if (isset($_POST['delete'])  == TRUE) {
    $id = (int)$_POST['delete'];
    if ($user->deleteUser($id)) {
        header("location: userView.php");
    }

}