<?php
include ('session.php');
include_once('vendor/autoload.php');

use Mess\DB\DB;
use Mess\Bill\bill;

DB::connect();

$bill = new Bill(DB::$conn);
$bill = $bill->edit($_POST);

require_once('Header.php');
?>


<div class="row">
    <div class="mainpage">
        <section class="col-md-8 col-md-offset-2">
            <form action="billUpdate.php" method="post">
                <input type="hidden" name="id" value="<?php echo $bill['id']?>">
                <table class="table table-bordered table-striped ">
                    <tr><td colspan="4"><h2 style="text-align: center">EDIT : BILL</h2></td></tr>
                    <tr>
                        <td>Banalce BF</td>
                        <td><input type="text" name="balance_bf" value="<?php echo $bill['balance_bf'] ?>"</td>

                        <td>Maintenance</td>
                        <td><input type="text" name="maintenance" value="<?php echo $bill['maintenance'] ?>"></td>
                    </tr>
                    <tr>
                        <td>Entertainment</td>
                        <td><input type="text" name="entertainment" value="<?php echo $bill['entertainment'] ?>"></td>

                        <td>Mess Sports</td>
                        <td><input type="text" name="mess_sports" value="<?php echo $bill['mess_sports'] ?>"></td>
                    </tr>
                    <tr>
                        <td>Garden</td>
                        <td><input type="text" name="garden" value="<?php echo $bill['garden'] ?>"></td>

                        <td>CO'S BF</td>
                        <td><input type="text" name="cos_bf" value="<?php echo $bill['cos_bf'] ?>"></td>
                    </tr>
                    <tr>
                        <td>CSF</td>
                        <td><input type="text" name="csf" value="<?php echo $bill['csf'] ?>"></td>

                        <td>CRF</td>
                        <td><input type="text" name="crf" value="<?php echo $bill['crf'] ?>"></td>
                    </tr>
                    <tr>
                        <td>CWC</td>
                        <td><input type="text" name="cwc" value="<?php echo $bill['cwc'] ?>"></td>

                        <td>Base Sports</td>
                        <td><input type="text" name="base_sports" value="<?php echo $bill['base_sports'] ?>"></td>
                    </tr>
                    <tr>
                        <td>BAFWWA</td>
                        <td><input type="text" name="bafwwa" value="<?php echo $bill['bafwwa'] ?>"></td>

                        <td>Barrack Damage</td>
                        <td><input type="text" name="barack_damage" value="<?php echo $bill['barack_damage'] ?>"></td>
                    </tr>
                    <tr>
                        <td>Wild Converstion Fund</td>
                        <td><input type="text" name="wild_fund" value="<?php echo $bill['wild_fund'] ?>"></td>

                        <td>FWS</td>
                        <td><input type="text" name="fws" value="<?php echo $bill['fws'] ?>"></td>
                    </tr>
                    <tr>
                        <td>BHSS</td>
                        <td><input type="text" name="bhss" value="<?php echo $bill['bhss'] ?>"></td>

                        <td>Levey / Party</td>
                        <td><input type="text" name="levey" value="<?php echo $bill['levey'] ?>"></td>
                    </tr>
                    <tr>
                        <td>CNF Loan</td>
                        <td><input type="text" name="cnf_loan" value="<?php echo $bill['cnf_loan'] ?>"></td>

                        <td>Casual Meal</td>
                        <td><input type="text" name="casual_meal" value="<?php echo $bill['casual_meal'] ?>"></td>
                    </tr>
                    <tr>
                        <td>Internet Bill</td>
                        <td><input type="text" name="internet" value="<?php echo $bill['internet'] ?>"></td>

                        <td>Others</td>
                        <td><input type="text" name="others" value="<?php echo $bill['others'] ?>"></td>
                    </tr>


                </table>
                <p style="text-align: center;"><button class="btn btn-primary" type="submit">Update</button></p>
            </form>
        </section>

    </div>
</div>

<?php
require_once('Footer.php');
?>
