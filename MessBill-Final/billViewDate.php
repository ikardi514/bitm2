<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\Utility\Setting;
use Mess\DB\DB;
use Mess\Bill\bill;

Setting::init();
DB::connect();

$bill= new Bill(DB::$conn);
$bills = $bill->getAllBillsByDate($_POST);

require_once('Header.php');

?>

<?php
if(isset($_SESSION['message'])){
    echo "<div class='alert alert-success col-md-8 col-md-offset-2 text-center'> ".$_SESSION['message']."</div>";
    unset($_SESSION['message']);
}
?>


<div class="row">
    <div class="mainpage ">
    <section class="col-md-12 allDataTable">
        <table class="table table-striped table-bordered">
            <tr><td colspan="23"><h2 style="text-align: center">SEARCH RESULT : BILL</h2></td></tr>
            <tr>
                <th>BD No</th>
                <th>BL</th>
                <th>Maint</th>
                <th>Ent</th>
                <th>Mess-Sports</th>
                <th>Garden</th>
                <th>Co's BF</th>
                <th>CSF</th>
                <th>CRF</th>
                <th>CWC</th>
                <th>Base-Sports</th>
                <th>BAFWWA</th>
                <th>Brk-dmg</th>
                <th>Wild</th>
                <th>FWS</th>
                <th>BHSS</th>
                <th>Party</th>
                <th>LOAN</th>
                <th>Meal</th>
                <th>Internet</th>
                <th>Others</th>
                <th>Total</th>
                <th>Action</th>
            </tr>
            <?php
            foreach($bills as $bill){
                ?>
            <tr>
               <td> <?php echo $bill['bdno'];?></td>
                    <td> <?php echo $bill['balance_bf'];?></td>
                    <td> <?php echo $bill['maintenance'];?></td>
                    <td> <?php echo $bill['entertainment'];?></td>
                    <td> <?php echo $bill['mess_sports'];?></td>
                    <td> <?php echo $bill['garden'];?></td>
                    <td> <?php echo $bill['cos_bf'];?></td>
                    <td> <?php echo $bill['csf'];?></td>
                    <td> <?php echo $bill['crf'];?></td>
                    <td> <?php echo $bill['cwc'];?></td>
                    <td> <?php echo $bill['base_sports'] ?> </td>
                    <td> <?php echo $bill['bafwwa'] ?> </td>
                    <td> <?php echo $bill['barack_damage'] ?> </td>
                    <td> <?php echo $bill['wild_fund'] ?> </td>
                    <td> <?php echo $bill['fws'] ?> </td>
                    <td> <?php echo $bill['bhss'] ?> </td>
                    <td> <?php echo $bill['levey'] ?> </td>
                    <td> <?php echo $bill['cnf_loan'] ?> </td>
                    <td> <?php echo $bill['casual_meal'] ?> </td>
                    <td> <?php echo $bill['internet'] ?> </td>
                    <td> <?php echo $bill['others'] ?> </td>
                    <td> <?php echo $bill['total'] ?> </td>
                    <td>
                        <form action="billEdit.php" method="post">
                            <input type="hidden" name="id"
                                   value="<?php echo $bill['id']?>">
                            <button type="submit"><img src="images/edit.png" title="Edit"/></button>
                        </form>

                        <form action="billShowPDF.php" method="post">
                            <input type="hidden" name="bdno"
                                   value="<?php echo $bill['bdno']?>">
                            <button type="submit"><img src="images/pdf.png" title="Print"/></button>
                        </form>

                    </td>
            </tr>
        <?php
        }
        ?>
        </table>

		
<!--        <h2 style="text-align: right">-->
<!--        <form action="billViewDatePDF.php" method="post" >-->
<!--            <input type="hidden" class="form-control" name="formDate">-->
<!--            <input type="hidden" class="form-control" name="toDate" value="">-->
<!--            <button type="submit"><img src="images/pdf.png" title="Download as PDF"></button>-->
<!--        </form></h2>-->
    </section>
</div>
</div>
<?php
require_once('Footer.php');
?>
