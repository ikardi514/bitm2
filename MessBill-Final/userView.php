<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\Utility\Setting;
use Mess\DB\DB;
use Mess\User\user;

Setting::init();
DB::connect();

$user= new User(DB::$conn);
$users = $user->getAllUsers();

require_once('Header.php');

?>


<div class="row">
    <div class="mainpage">

        <?php
        if(isset($_SESSION['message'])){
            echo "<div class='alert alert-success col-md-8 col-md-offset-2 text-center'> ".$_SESSION['message']."</div>";
            unset($_SESSION['message']);
        }
        ?>


    <section class="col-md-8 col-md-offset-2">
        <table class="table table-striped table-bordered">
            <tr><td colspan="4"><h2 style="text-align: center">ALL USERS</h2></td></tr>
            <tr>
                <th>User Name</th>
                <th>Password</th>
                <th>Email</th>
                <th>Action</th>
            </tr>

           
			<?php
				foreach($users as $user){
            ?>
            
                <tr>
                    <td> <?php echo $user['user_name'] ?> </td>
                    <td> <?php echo $user['password'] ?> </td>
                    <td> <?php echo $user['email'] ?> </td>
                    <td>

                        <div class="iconInline">

                            <a href="userEdit.php?id=<?php echo $user['id'];?>"><button  type="submit"><img src="images/edit.png" title="Edit"/></button></a>

                        </div>




                        <div class="iconInline">
                            <form action="userDelete.php" method="post">
                                <input  type="hidden" name="delete"  value="<?php echo $user['id'];?>"/>
                                <button  type="submit"><img src="images/delete.png" title="Delete" /></button>
                            </form>
                        </div>
                        <div class="iconInline">
                            <a href="userShow.php?id=<?php echo $user['id'];?>"><button  type="submit"><img src="images/show.png" title="Show" /></button></a>
                        </div>
                        </td>

                </tr>

                <?php
            }
            ?>
        </table>
    </section>
</div>
</div>
<?php
require_once('Footer.php');
?>
