<?php
include_once ('vendor/autoload.php');

use Mess\DB\DB;
use Mess\Bill\bill;

DB::connect();
$bill= new Bill(DB::$conn);
$bill = $bill->showPDF($_POST);

date_default_timezone_set('Asia/Dacca');
$datetime = new \DateTime();
$date = $datetime->format('d-M-Y H:i:s');

$content1 = <<<BITMP1

                <h3 style="text-align: center"> <img src="images/baf.png"/> &nbsp; Mess Bill</h3>

                <h4 style="text-align: center">BAF BASE BASHAR</h4><p align='center'>Report generated on : $date</p>
        <table border="1" style="border-collapse: collapse;"  cellspacing="4" cellpadding="4" width="100%" align="center" >
BITMP1;
  $content2="";
$content2.="<tr><td>BD No </td><td>".$bill['bdno']."</td>
            <td>Name </td><td>".$bill['name']."</td></tr>
            <tr><td>Rank </td><td>".$bill['rank']."</td>
            <td>Trade </td><td>".$bill['trade']."</td></tr>
            </table><br />";


$content3="";
$content3.="<table border='1' style='border-collapse: collapse;'  cellspacing='4' cellpadding='4' width='100%' align='center'>
<tr><td><b>Particulars</b></td><td><b>Taka</b></td><td><b>Particulars</b></td><td><b>Taka</b></td></tr>

<tr><td>Balance BF</td><td>".$bill['balance_bf']."</td>
            <td>Maintenance</td><td>".$bill['maintenance']."</td></tr>

            <tr><td>Entertainment</td><td>".$bill['entertainment']."</td>
            <td>Mess Sports</td><td>".$bill['mess_sports']."</td></tr>

            <tr><td>Garden</td><td>".$bill['garden']."</td>
            <td>CO'S BF</td><td>".$bill['cos_bf']."</td></tr>

            <tr><td>CSF</td><td>".$bill['csf']."</td>
            <td>CRF</td><td>".$bill['crf']."</td></tr>

            <tr><td>CWC</td><td>".$bill['cwc']."</td>
            <td>Base Sports</td><td>".$bill['base_sports']."</td></tr>

            <tr><td>BAFWWA</td><td>".$bill['bafwwa']."</td>
            <td>Barrack Damage</td><td>".$bill['barack_damage']."</td></tr>

            <tr><td>Wild Converstion Fund</td><td>".$bill['wild_fund']."</td>
            <td>FWS</td><td>".$bill['fws']."</td></tr>

            <tr><td>BHSS</td><td>".$bill['bhss']."</td>
            <td>Levey / Party</td><td>".$bill['levey']."</td></tr>

            <tr><td>CNF Loan</td><td>".$bill['cnf_loan']."</td>
            <td>Casual Meal</td><td>".$bill['casual_meal']."</td></tr>

            <tr><td>Internet Bill</td><td>".$bill['internet']."</td>
            <td>Others</td><td>".$bill['others']."</td></tr>

            <tr><td colspan='3'>Total Bill:</td><td>".$bill['total']."</td></tr>";

$content4.= "</table>


<p style='text-align: left'> Date:</p><p style='text-align: right;'> <b>Treasure Sgt's Mess</b></p>
<p style='text-align: left'>NB: Mess bill will be paid in full by 10th of every month. Over/Less charges if any will be adjusted in the next month</p>
";

$mpdf = new mPDF();
$mpdf->WriteHTML($content1.$content2.$content3.$content4);
$mpdf->Output();

?>
