<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\DB\DB;
use Mess\Member\member;

DB::connect();

$member= new Member(DB::$conn);
$member = $member->searchMember($_POST);

require_once('Header.php');
?>



    <form action="billStore.php" method="post">
        <input type="hidden" name="member_id" value="<?php echo $member['id']?>" />
        <div class="row">
            <section class="col-md-8 col-md-offset-2">

                <table class="table table-striped table-bordered">
                    <tr><td colspan="4"><h2 style="text-align: center">CREATE BILL</h2></td></tr>
                    <tr>
                        <td>BD Number</td>
                        <td><input class="form-control" type="text"  readonly="readonly" value="<?php echo $member['bdno']?>" /></td>
                        <td>Month</td>
                        <td><input  type="Date"  name="billMonth" class="form-control" disabled></td>
                    </tr>

                    <tr>
                        <td>Balance BF</td>
                        <td><input class="form-control" type="text" name="balance_bf"> </td>
                        <td>Maintenance</td>
                        <td><input  type="text"   name="maintenance" class="form-control">  </td>
                    </tr>

                    <tr>
                        <td>Entertainment</td>
                        <td><input  type="text" value="2" name="entertainment" class="form-control"></td>
                        <td>Mess Sports</td><td>
                            <input  type="text" value="3" name="mess_sports" class="form-control"></td>
                    </tr>

                    <tr>
                        <td>Garden</td>
                        <td><input  type="text" value="1" name="garden" class="form-control"></td>
                        <td>CO'S BF</td>
                        <td><input  type="text" value="12" name="cos_bf" class="form-control"></td>
                    </tr>

                    <tr>
                        <td>CSF</td>
                        <td><input  type="text" value="16" name="csf" class="form-control"></td>
                        <td>CRF</td>
                        <td><input  type="text" value="10" name="crf" class="form-control"></td>
                    </tr>

                    <tr>
                        <td>CWC</td>
                        <td><input  type="text" value="7" name="cwc" class="form-control"></td>
                        <td>Base Sports</td>
                        <td><input  type="text"  value="1" name="base_sports" class="form-control"></td>
                    </tr>

                    <tr>
                        <td>BAFWWA</td>
                        <td><input  type="text" value="1" name="bafwwa" class="form-control"></td>
                        <td>Barrack Damage</td>
                        <td><input  type="text" value="1" name="barack_damage" class="form-control"></td>
                    </tr>

                    <tr>
                        <td>Wild Converstion Fund</td>
                        <td><input  type="text" value="10" name="wild_fund" class="form-control"></td>
                        <td>FWS</td>
                        <td><input  type="text"  name="fws" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>BHSS</td>
                        <td><input  type="text" name="bhss" class="form-control"></td>
                        <td>Levey / Party</td>
                        <td><input  type="text"  name="levey" class="form-control"></td>
                    </tr>

                    <tr>
                        <td>CNF Loan</td>
                        <td><input  type="text" name="cnf_loan" class="form-control"></td>
                        <td>Casual Meal</td>
                        <td><input  type="text"  name="casual_meal" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Internet Bill</td>
                        <td><input  type="text"  name="internet" class="form-control"></td>
                        <td>Others</td>
                        <td><input  type="text"  name="others" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="3"><button type="submit"  name="sub" class="btn btn-primary">Create Bill</button> &nbsp; <button type="reset" class="btn btn-danger">Clear</button></td>
                    </tr>
                </table>

            </section>
        </div>
    </form>



<?php
require_once('Footer.php');
?>