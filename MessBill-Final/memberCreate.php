<?php
    include ('session.php');
    require_once('Header.php');
?>

    <form action="memberStore.php" method="post">
        <div class="row">
            <div class="mainpage">
                <?php
                if(isset($_SESSION['message'])){
                    echo "<div class='alert alert-danger col-md-6 col-md-offset-4 text-center'> ".$_SESSION['message']."</div>";
                    unset($_SESSION['message']);
                }
                ?>
                <section>
                    <h2 style="text-align: center">ADD MESS MEMBER</h2><br />
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="bdno" class="col-md-2 col-md-offset-2 control-label">BD Number</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" id="bdno" placeholder="BD Number" name="bdno" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 col-md-offset-2 control-label">Rank</label>
                            <div class="col-md-6">
                                <select required class="form-control" name="rank" required>
                                    <option value="">Please select</option>
                                    <option value="MWO">MWO</option>
                                    <option value="SWO">SWO</option>
                                    <option value="WO">WO</option>
                                    <option value="Sgt">Sgt</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-2 col-md-offset-2 control-label">Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="name" placeholder="Name" name="name" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-2 col-md-offset-2 control-label">Trade</label>
                            <div class="col-md-6">
                                <select class="form-control" name="trade" required>
                                    <option value="">Please select</option>
                                    <option value="Radio">Radio</option>
                                    <option value="GS">GS</option>
                                    <option value="Radar">Radar</option>
                                    <option value="Clerk">Clerk</option>
                                    <option value="Engine">Engine</option>
                                    <option value="AC Fitt">AC Fitt</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="entryno" class="col-md-2 col-md-offset-2 control-label">Entry No</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" id="entryno" placeholder="Entry No" name="entryno" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button class="btn btn-primary" type="submit" name="sub">ADD MEMBER</button>
								<button class="btn btn-danger" type="reset" name="sub">CLEAR</button>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </form>


<?php
require_once('Footer.php');
?>