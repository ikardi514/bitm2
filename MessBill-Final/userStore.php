<?php
include ('session.php');
include_once ('vendor/autoload.php');

use Mess\Utility\Setting;
use Mess\Utility\Sanitize;
use Mess\DB\DB;
use Mess\User\user;

Setting::init();
DB::connect();

?>

<?php
$data = Sanitize::sanitize($_POST);
$user = new User(DB::$conn);
$user->store($data);

?>

