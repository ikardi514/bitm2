<?php
include ('session.php');
include_once('vendor/autoload.php');

use Mess\Utility\Setting;
use Mess\Utility\Sanitize;
use Mess\DB\DB;
use Mess\Bill\bill;

Setting::init();
DB::connect();

?>

<?php
$data = Sanitize::sanitize($_POST);
$bill = new Bill(DB::$conn);
//var_dump($data);
$total = $data['balance_bf'] + $data['maintenance'] + $data['entertainment'] + $data['mess_sports'] + $data['garden'] + $data['cos_bf']
    + $data['csf'] + $data['crf'] + $data['cwc'] + $data['base_sports'] + $data['bafwwa'] + $data['barack_damage']
    + $data['wild_fund'] + $data['fws'] + $data['bhss'] + $data['levey'] + $data['cnf_loan'] + $data['casual_meal']
    + $data['internet'] + $data['others'];

echo $data['total']= $total;
$bill->update($data);
//var_dump($_POST);
?>

